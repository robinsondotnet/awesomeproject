import Vue from 'vue'
import VueRouter from 'vue-router'
import ProjectsPage from './pages/ProjectsPage.vue'
import FavProjectsPage from './pages/FavProjectsPage'
import LoginPage from './pages/LoginPage.vue'
import store from './store/store';

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
        { path: '/projects', name: 'projects', component: ProjectsPage, meta: { requiresAuth: true } },
        { path: '/projects/fav', name: 'fav-projects', component: FavProjectsPage, meta: { requiresAuth: true } },
        { path: '*', name: 'home', redirect: '/projects', meta: { requiresAuth: true } },
        { path: '/login', name: 'login', component: LoginPage }
    ]
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {

        if (!store.getters.isLoggedIn) {
               next({
                 path: '/login',
                 query: { redirect: to.fullPath }
               })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})

export default router
