export default {
    state() {
        return {
            projects: [
                {
                    id: 1,
                    name: "AwesomeNodeProject",
                    description: "TBD",
                    roles: [{ name: "Frontend", capacity: 5, remainig: 1 }],
                    skills: [
                        { name: "VueJS" },
                        { attached: true, name: "Seniority", value: "Senior A+" }
                    ]
                },
                {
                    id: 2,
                    name: "AwesomeDotnetProject",
                    description: "TBD",
                    roles: [{ name: "Backend", capacity: 6, remainig: 2 }],
                    skills: [
                        { name: ".NET" },
                        { attached: true, name: "Seniority", value: "Engineer 3" }
                    ]
                }
            ]
        }
    }
}
