import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILED, LOGOUT } from './constants'
import projectsModule from './modules/projects'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('jwt') || '',
        user: {},
        status: ''
    },
    getters: {
        isLoggedIn: state => !!state.token
    },
    actions: {
        login({ commit }, user) {
            return new Promise((resolve, reject) => {
                commit(LOGIN_REQUEST)

                axios.post('http://localhost:3000/auth/login', user)
                .then(response => {
                    const { token } = response.data
                    commit(LOGIN_SUCCESS, token)
                    resolve(response)
                })
                .catch(err => {
                    console.log(err)
                    commit(LOGIN_FAILED)
                    reject(err)
                })
            })
        },
        register({ commit }, user) {

        },
        logout({ commit }) {
            return new Promise((resolve, reject) => {
                commit(LOGOUT)
                resolve({})
            })
        }
    },
    mutations: {
        [LOGIN_SUCCESS] (state, token) {
            state.user = { name: 'kento'}
            state.token = token
            localStorage.setItem('jwt', token)
        },
        [LOGIN_REQUEST] (state) {
            console.log('login request started')
        },
        [LOGIN_FAILED] (state) {
            console.log('login failed')
        },
        [LOGOUT] (state) {
            console.log('commit logout')
            state.token = ''
            state.user = {}
            localStorage.removeItem('jwt')
        }
    },
    modules: {
        projects: projectsModule
    }
})

export default store
